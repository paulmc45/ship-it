(function (ajs) {
    ajs.$(document).on('click', '#alert', function () {
        ajs.flag({
            type: 'success',
            body: 'Added ability to create clustered product instances via amps!',
            close: 'auto'
        })
    });
})(AJS || $);
