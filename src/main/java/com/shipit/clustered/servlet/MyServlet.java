package com.shipit.clustered.servlet;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

public class MyServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(MyServlet.class);

    private final SoyTemplateRenderer templateRenderer;

    public MyServlet(@ComponentImport final SoyTemplateRenderer templateRenderer) {
        this.templateRenderer = templateRenderer;
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        // resp.setContentType("text/html");
        // resp.getWriter().write("<html><body>Hello World</body></html>");

        templateRenderer.render(
                resp.getWriter(),
                "com.shipit.clustered.shipit:plugin-templates",
                "plugin.init",
                Collections.emptyMap()
        );

    }

}