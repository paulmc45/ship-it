package com.shipit.clustered.api;

public interface MyPluginComponent
{
    String getName();
}